/**
 * Hva skrives ut før og etter?​
 */

public class Oppgave2 {
    public static void main(String[] args) {
        System.out.println("Oppgave 2");

        int a = 10, b = 20;
        System.out.println("Før: " + a + " " + b);

        RubbishAndNonsense2 rubbishAndNonsense = new RubbishAndNonsense2();
        rubbishAndNonsense.swapNumbers(a, b);
        System.out.println("Etter: " + a + " " + b);

        // TODO Run and inspect før and etter print statements! Do the numbers get swapped?
    }
}

class RubbishAndNonsense2 {
    public void swapNumbers(int t1, int t2) {
        int hjelp = t1;
        t1 = t2;
        t2= hjelp;
    }
}