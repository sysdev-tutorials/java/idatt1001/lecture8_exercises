class Oppgave5 {
    public static void main(String[] args) {
        new Oppgave5().oggave5();
    }

    private void oggave5(){
        Person p1 = new Person("Ole", 1980);
        Person p2 = new Person("Kari", 1984);

        // TODO inpsect following print statements
        System.out.println("p1 and p1 have age: " + p1.isSameAge(p2));
        System.out.println("p1 is older than p2: " + p1.compareTo(p2));
        System.out.println("p1 is 4 years older than p2: " + p1.olderThan(4, p2));
        System.out.println("p1 and p2 are equal: " + p1.equals(p2));
    }
}

class Person {
    
    private final String name;
    private final int yearOfBirth;

    public Person (String name, int yearOfBirth){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    // 5.1
    public boolean isSameAge(Person anotherPerson){
        // TODO implement if this(person) and anotherPerson have same age (i.e yearOfBirth)
    }

    //2 
    public int compareTo(Person anothPerson) {
        /** 
         * TODO the implement following logic
         * if this(person) is older than anotherPerson return 1
         * if this(person) has same age as anotherPerson return 0
         * else return -1
         */
    }

    // 3
    public boolean olderThan(int x, Person anotherPerson) {
        /**
         * TODO the implement following logic
         * return true if this(person) is x years older than anotherPerson, else return false
         */
    }

    // 4
    @Override
    public boolean equals(Object anotherPerson) {

        /**
         * TODO Override default behavior of equals method
         * check if anotherPerson is of type Person
         * compare if this(person) and anotherPerson are same i.e same reference
         * also compare both name and yearOfBirth for this(person) and anotherPerson
         */
        
    }
}

