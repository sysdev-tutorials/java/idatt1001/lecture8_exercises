
/**
 * 
 */
public class Oppgave1 {

    public static void main(String[] args) {
        
        System.out.println("Oppgave 1");

        int tall1 = 3;
        int tall2 = 4;
        System.out.println("Før: " + tall1 + " " + tall2);

        int hjelp = tall1;
        tall1 = tall2;
        tall2 = hjelp;
        System.out.println("Etter: " + tall1 + " " + tall2);

        // TODO Run and inspect før and etter print statements! Do the numbers get swapped?
    }
}
