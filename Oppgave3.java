/**
 * 
 * Hva skrives ut før og etter ved bruk av methoder swapObjects1 og swapObjects2?​
 */


public class Oppgave3 {
    public static void main(String[] args) {
      new Oppgave3().oppgave3_1();
      new Oppgave3().oppgave3_2();
    }

    private void oppgave3_1(){
      System.out.println("swapObjects1");

      // TODO
      // instantiate name1 and name2 objects from Name class

      // print name1 and name 2

      // instiate rubbishAndNonsense object from RubbishAndNonsense3 class

      // call rubbishAndNonsense.swapObjects1(name1, name2);

      // print name1 and name 2

      // inspect før and etter print results above. Do the names get swapped?
  }

  private void oppgave3_2(){
      System.out.println("swapObjects2");


      // TODO
      // instantiate name1 and name2 objects from Name class

      // (før) print name1 and name 2

      // instiate rubbishAndNonsense object from RubbishAndNonsense3 class

      // call rubbishAndNonsense.swapObjects2(name1, name2);

      // (etter) print name1 and name 2

      // inspect før and etter print results above. Do the names get swapped?
  }
}

class RubbishAndNonsense3 {

    public void swapObjects1 (Name f1, Name f2){
        Name help = f1;
        f1 = f2;
        f2 = help;
    }

    public void swapObjects2 (Name f1, Name f2){
        Name help = new Name (f1.getFirstname(), f1.getSurname());
        f1 = new Name(f2.getFirstname(), f2.getSurname());
        f2 = help;
    }
}

class Name {
    private String firstname;
    private String surname;

    public Name (String firstname, String surname) {
      this.firstname = firstname;
      this.surname = surname;
    }
    public void setFirstname(String newValue) {
      firstname = newValue;
    }
    public void setSurname(String newValue) {
      surname = newValue;
    }
    public String getFirstname() {
        return firstname;
      }
    public String getSurname() {
        return surname;
      }
    public String toString() {  
        return firstname + " " + surname;
      }
}
