/**
* 
*/
public class Oppgave4 {
    public static void main(String[] args) {
        new Oppgave4().oppgave4_1();
        new Oppgave4().oppgave4_2();
    }

    private void oppgave4_1(){
        System.out.println("oppgave3_3");
        RubbishAndNonsense4 rubbishAndNonsense = new RubbishAndNonsense4();
        Name name1 = new Name("Ole", "Normann");
        Name name2 = new Name("Kari", "Grossmann");

        System.out.println("Før: " + name1 + " " + name2);
        rubbishAndNonsense.swapObjects3(name1, name2);
        System.out.println("Før: " + name1 + " " + name2);

        // TODO
        // instantiate name1 and name2 objects from Name class

        // print name1 and name 2

        // instiate rubbishAndNonsense object from RubbishAndNonsense4 class

        // call rubbishAndNonsense.swapObjects3(name1, name2);

        // print name1 and name 2

        // inspect før and etter print results above. Do the names get swapped?    
    }

    private void oppgave4_2(){
        System.out.println("oppgave4");

        // TODO
        // instantiate name1 and name2 objects from Name class

        // print name1 and name 2

        // instiate rubbishAndNonsense object from RubbishAndNonsense4 class

        // call rubbishAndNonsense.swapObjects4(name1, name2);

        // print name1 and name 2

        // inspect før and etter print results above. Do the names get swapped?    
    } 

}


class RubbishAndNonsense4 {

    public void swapObjects3 (Name f1, Name f2){
        Name help = f1;
        f1.setFirstname(f2.getFirstname());
        f1.setSurname(f2.getSurname());
        f2.setFirstname(help.getFirstname()); 
        f2.setSurname(help.getSurname());
    }

    public void swapObjects4 (Name f1, Name f2){
        Name help = new Name(f1.getFirstname(), f1.getSurname());
        f1.setFirstname(f2.getFirstname());
        f1.setSurname(f2.getSurname());
        f2.setFirstname(help.getFirstname()); 
        f2.setSurname(help.getSurname());
    }
}